package com.adit.tesseracttextrecognition.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.adit.tesseracttextrecognition.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.text.Text;
import com.google.mlkit.vision.text.TextRecognition;
import com.google.mlkit.vision.text.TextRecognizer;
import com.googlecode.tesseract.android.TessBaseAPI;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ResultFragment extends Fragment implements CropImageView.OnSetImageUriCompleteListener, CropImageView.OnCropImageCompleteListener {

    @BindView(R.id.tvUri)
    TextView tvUri;

    @BindView(R.id.imagePreview)
    ImageView imagePreview;

    @BindView(R.id.cropImageView)
    CropImageView cropImageView;

    @BindView(R.id.tvTessOcrTextResult)
    TextView tvTessOcrTextResult;

    @BindView(R.id.btnCrop)
    Button btnCrop;

    @BindView(R.id.btnTessTryDetect)
    Button btnTessTryDetect;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.tilNik)
    TextInputLayout tilNik;

    @BindView(R.id.tilNama)
    TextInputLayout tilNama;

    @BindView(R.id.tilTTL)
    TextInputLayout tilTTL;

    @BindView(R.id.tilJenisKelamin)
    TextInputLayout tilJenisKelamin;

    @BindView(R.id.tilAlamat)
    TextInputLayout tilAlamat;

    @BindView(R.id.tilAgama)
    TextInputLayout tilAgama;

    @BindView(R.id.tilStatusPerkawinan)
    TextInputLayout tilStatusPerkawinan;

    @BindView(R.id.tilPekerjaan)
    TextInputLayout tilPekerjaan;

    @BindView(R.id.tilKewarganegaraan)
    TextInputLayout tilKewarganegaraan;


    @BindView(R.id.tieNik)
    TextInputEditText tieNik;

    @BindView(R.id.tieNama)
    TextInputEditText tieNama;

    @BindView(R.id.tieTTL)
    TextInputEditText tieTTL;

    @BindView(R.id.tieJenisKelamin)
    TextInputEditText tieJenisKelamin;

    @BindView(R.id.tieAlamat)
    TextInputEditText tieAlamat;

    @BindView(R.id.tieAgama)
    TextInputEditText tieAgama;

    @BindView(R.id.tieStatusPerkawinan)
    TextInputEditText tieStatusPerkawinan;

    @BindView(R.id.tiePekerjaan)
    TextInputEditText tiePekerjaan;

    @BindView(R.id.tieKewarganegaraan)
    TextInputEditText tieKewarganegaraan;

    private String uri;
    private Handler handler;
    private Runnable handlerTask;
    private Bitmap bmp;

    public ResultFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_result, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        cropImageView.setOnSetImageUriCompleteListener(this);
        cropImageView.setOnCropImageCompleteListener(this);
        uri = ResultFragmentArgs.fromBundle(getArguments()).getImageUri();
        bmp = ResultFragmentArgs.fromBundle(getArguments()).getBitmap();

        if(uri.isEmpty()){
            tvUri.setText("URI is empty!");
        }else{
            tvUri.setText(uri);
        }


//        initBmp();
//        initImageCrop();
        bmp = initGrayScaleBitmap(bmp, 5.0f, 1);

        imagePreview.setImageBitmap(bmp);

        btnTessTryDetect.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);
        tvTessOcrTextResult.setText("memproses gambar...");
        handler = new Handler();

        handlerTask = new Runnable() {

            @Override
            public void run() {
                //detectText(bmp);
                detectTextMLKit(bmp);
            }
        };

        handler.postDelayed(handlerTask, 2000);
//        detectText(bmp);
    }

    @Override
    public void onCropImageComplete(CropImageView view, CropImageView.CropResult result) {
        getCroppedImage(result);
    }

    @Override
    public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error) {
        if (error == null) {
            Toast.makeText(getContext(), "Image load successful", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getContext(), "Image load failed: " + error.getMessage(), Toast.LENGTH_LONG)
                    .show();
        }
    }

    private Bitmap initGrayScaleBitmap(Bitmap bitmap, float contrast, float brightness){
        Bitmap grayscaleBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(grayscaleBitmap);
        Paint paint = new Paint();
        ColorMatrix colorMatrix = new ColorMatrix();

        ColorMatrix cm = new ColorMatrix(new float[]{
                        contrast, 0, 0, 0, brightness,
                        0, contrast, 0, 0, brightness,
                        0, 0, contrast, 0, brightness,
                        0, 0, 0, 1, 0
        });

        colorMatrix.setSaturation(0);
//        cm.setSaturation(0);

        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(colorMatrix);
        paint.setColorFilter(filter);
        c.drawBitmap(bitmap, 0,0, paint);

        return grayscaleBitmap;

    }

    private void initBmp(){

        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inJustDecodeBounds = false;
        opt.inSampleSize = 6;

        // bmp method
        Bitmap bmp = BitmapFactory.decodeFile(uri, opt);

        try {
            ExifInterface exif = new ExifInterface(uri);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
            Matrix matrix = new Matrix();

            if(orientation == 6){
                matrix.postRotate(90);
            }else if(orientation == 3){
                matrix.postRotate(180);
            }else if(orientation == 8){
                matrix.postRotate(270);
            }

            bmp = Bitmap.createBitmap(bmp, 0,0, bmp.getWidth(), bmp.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }

//        setInitialCropRect();
        imagePreview.setImageBitmap(bmp);
//        ocr(bmp);
        detectText(bmp);
    }

    public void initImageCrop(){
        //cropImageView.setImageUriAsync(Uri.parse(uri));


        //using bitmap
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inJustDecodeBounds = false;
        opt.inSampleSize = 6;

        // bmp method
        Bitmap bmp = BitmapFactory.decodeFile(uri, opt);

        try {
            ExifInterface exif = new ExifInterface(uri);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
            Matrix matrix = new Matrix();

            if(orientation == 6){
                matrix.postRotate(90);
            }else if(orientation == 3){
                matrix.postRotate(180);
            }else if(orientation == 8){
                matrix.postRotate(270);
            }

            bmp = Bitmap.createBitmap(bmp, 0,0, bmp.getWidth(), bmp.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        cropImageView.setImageBitmap(bmp);
        cropImageView.setCropRect(new Rect(100, 300, 500, 1200));

    }

    @OnClick(R.id.btnCrop)
    public void cropImage(){
        cropImageView.getCroppedImageAsync();
    }

    private void getCroppedImage(CropImageView.CropResult result){
        Bitmap resultBmp = result.getBitmap();
        imagePreview.setImageBitmap(resultBmp);

        cropImageView.setVisibility(View.GONE);
        btnCrop.setVisibility(View.INVISIBLE);
        detectText(resultBmp);
    }

    /*
    public void ocr(Bitmap bmp){
        TessBaseAPI tessBaseAPI = new TessBaseAPI();

//        String path = getExternalFilesDir(null).getAbsolutePath() + "/tesseract/";
        String path = Environment.getExternalStorageDirectory() + "/tesseract";

        Toast.makeText(getActivity(), "Tesseract path is : " + path, Toast.LENGTH_LONG).show();

        tessBaseAPI.init(path,"eng");
        tessBaseAPI.setPageSegMode(TessBaseAPI.PageSegMode.PSM_SINGLE_LINE);
//        tessBaseAPI.setPageSegMode(TessBaseAPI.PageSegMode.PSM_AUTO);
        tessBaseAPI.setImage(bmp);

        String result = tessBaseAPI.getUTF8Text();

        System.out.println("OCR RESULT = " + result);
        Toast.makeText(getActivity(), "OCR RESULT : " + result, Toast.LENGTH_LONG).show();
        tvTessOcrTextResult.setText(result);
    }*/

    @OnClick(R.id.btnTessTryDetect)
    public void tryAgain(){
        btnTessTryDetect.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);
        tvTessOcrTextResult.setText("memproses gambar...");

        tilNik.setVisibility(View.GONE);
        tilNama.setVisibility(View.GONE);
        tilTTL.setVisibility(View.GONE);

        handler = new Handler();

        handlerTask = new Runnable() {

            @Override
            public void run() {
                //detectText(bmp);
                detectTextMLKit(bmp);
            }
        };

        handler.postDelayed(handlerTask, 2000);

//        detectText(bmp);
    }

    public void detectText(Bitmap bmp){
        TessBaseAPI tessBaseAPI = new TessBaseAPI();
        //String path = Environment.getExternalStorageDirectory() + "/tesseract";
        String path = getContext().getExternalFilesDir(null).getAbsolutePath() + "/tesseract/";
        String result = "Empty";

        tessBaseAPI.init(path,"eng");
//        tessBaseAPI.setPageSegMode(TessBaseAPI.PageSegMode.PSM_AUTO);
//        tessBaseAPI.setPageSegMode(TessBaseAPI.PageSegMode.PSM_AUTO_OSD);
        tessBaseAPI.setPageSegMode(TessBaseAPI.PageSegMode.PSM_SPARSE_TEXT);
//        tessBaseAPI.setPageSegMode(TessBaseAPI.PageSegMode.PSM_SINGLE_WORD);

        tessBaseAPI.setImage(bmp);

        try{
            result = tessBaseAPI.getUTF8Text();
        }catch (Exception e){
            Toast.makeText(getContext(), "EXCEPTION : " + e.getMessage(), Toast.LENGTH_LONG).show();
        }

        Toast.makeText(getActivity(), "OCR RESULT : " + result, Toast.LENGTH_LONG).show();
        tessBaseAPI.end();

        progressBar.setVisibility(View.INVISIBLE);
        tvTessOcrTextResult.setText(result);
        btnTessTryDetect.setVisibility(View.VISIBLE);
        btnTessTryDetect.setEnabled(true);

    }

    public void detectTextMLKit(Bitmap bmp){
        InputImage image = InputImage.fromBitmap(bmp, 0);

        TextRecognizer textRecognizer = TextRecognition.getClient();

        textRecognizer.process(image)
                .addOnSuccessListener(new OnSuccessListener<Text>() {
                    @Override
                    public void onSuccess(Text text) {

                        Toast.makeText(getContext(), "SUCCESS!", Toast.LENGTH_LONG).show();
                    }
                })

                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getContext(), "EXCEPTION : " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                })

                .addOnCompleteListener(new OnCompleteListener<Text>() {
                    @Override
                    public void onComplete(@NonNull Task<Text> task) {
                        Toast.makeText(getContext(), "COMPLETED!", Toast.LENGTH_LONG).show();
                        process(task);
                    }
                });
    }

    private void process(Task<Text> result){
        String resultText = result.getResult().getText();
        String blockText = "", lineText = "", elementText = "";

        for(Text.TextBlock block : result.getResult().getTextBlocks()){
            blockText += block.getText() + "\n";
//            Point[] blockCornerPoints = block.getCornerPoints();
//            Rect blockFrame = block.getBoundingBox();

            for(Text.Line line : block.getLines()){
                lineText += line.getText() + "\n";
//                Point[] lineCornerPoints = line.getCornerPoints();
//                Rect lineFrame = line.getBoundingBox();

                for(Text.Element element : line.getElements()){
                    elementText += element.getText() + "\n";
//                    Point[] elementCornerPoints = element.getCornerPoints();
//                    Rect elementFrame = element.getBoundingBox();
                }
            }
        }

//        tvTessOcrTextResult.setText("RESULT : \n" + resultText + "\n\n" +
//                "BLOCK TEXT : \n" + blockText + "\n\n" +
//                "LINE TEXT : \n" + lineText + "\n\n" +
//                "ELEMENT TEXT : \n" + elementText);


        String allText = "",
        nik = "",
        nama = "",
        ttl = "",
        jenisKelamin = "",
        agama = "",
        statusPerkawinan = "",
        kewarganegaraan = "",
        pekerjaan = "";

        String splitText[] = resultText.split("\n");
//        String splitText[] = elementText.split("\n");

        for(int i=0; i<splitText.length ; i++){
            allText += "[" + i + "] " + splitText[i]+"\n";

            //using elementText
            if(splitText[i].equalsIgnoreCase("nik")){
                nik = splitText[i+1];
            }

            if(splitText[i].equalsIgnoreCase("nama") || splitText[i].contains("nama")){
                nama = splitText[i+1];
            }

//            if(splitText[i].equalsIgnoreCase("nama") || splitText[i].contains("nama")){
//                nama = splitText[i+1];
//            }

        }

        //using result.gettext
//        nik = splitText[3];
//        nama = splitText[5] + " " + splitText[6];
//        ttl = splitText[7];

        ttl = splitText[12] + " " + splitText[13];


//        tvTessOcrTextResult.setText(allText + "\n\n" + blockText + "\n\n" + lineText + "\n\n" + elementText);

        tvTessOcrTextResult.setText(allText);


        tilNik.setVisibility(View.VISIBLE);
        tilNama.setVisibility(View.VISIBLE);
        tilTTL.setVisibility(View.VISIBLE);

        tieNik.setText(nik);
        tieNama.setText(nama);
        tieTTL.setText(ttl);

        progressBar.setVisibility(View.INVISIBLE);
        btnTessTryDetect.setVisibility(View.VISIBLE);
        btnTessTryDetect.setEnabled(true);
    }


}