package com.adit.tesseracttextrecognition.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.ExifInterface;
import android.media.Image;
import android.media.ImageReader;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.NavDirections;
import androidx.navigation.fragment.NavHostFragment;

import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.WindowMetrics;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.adit.tesseracttextrecognition.R;
import com.adit.tesseracttextrecognition.classes.CustomFrame;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainFragment extends Fragment {

    private static final int MAX_PREVIEW_WIDTH = 1920;
    private static final int MAX_PREVIEW_HEIGHT = 1080;

    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();

    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

//    static {
//        ORIENTATIONS.append(Surface.ROTATION_0, 0);
//        ORIENTATIONS.append(Surface.ROTATION_90, 90);
//        ORIENTATIONS.append(Surface.ROTATION_180, 180);
//        ORIENTATIONS.append(Surface.ROTATION_270, 270);
//    }

    @BindView(R.id.txtvCamPreview)
    TextureView textureView;

    @BindView(R.id.btnCapture)
    ImageButton btnCapture;

    @BindView(R.id.btnTest)
    ImageButton btnTest;

    @BindView(R.id.cameraView)
    SurfaceView cameraView;

    @BindView(R.id.transparentView)
    SurfaceView transparentView;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.imagePreview)
    ImageView imagePreview;

    @BindView(R.id.cropImageView)
    CropImageView cropImageView;

    private final TextureView.SurfaceTextureListener mSurfaceTextureListener =
            new TextureView.SurfaceTextureListener() {
                @Override
                public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                    openCamera(width, height);
                }

                @Override
                public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
                    configureTransform(width, height);
                }

                @Override
                public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                    return true;
                }

                @Override
                public void onSurfaceTextureUpdated(SurfaceTexture surface) {

                }
            };

    private String mCameraId;
    private TextureView mTextureView;
    private CameraCaptureSession mCaptureSession;
    private CameraDevice mCameraDevice;
    private Size mPreviewSize;
    private HandlerThread mBackgroundThread;
    private Handler mBackgroundHandler, mainHandler;
    private ImageReader mImageReader;
    private CaptureRequest.Builder mPreviewRequestBuilder;
    private CaptureRequest mPreviewRequest;
    private Semaphore mCameraOpenCloseLock = new Semaphore(1);
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private static final int REQUEST_WRITE_EXTERNAL_STORAGE = 2;
    private SurfaceHolder cameraViewHolder, transparentViewHolder;
    private CustomFrame customFrame;
    private Handler handler;
    private Runnable runnable;
    private Rect rect;

    //method 2 using surface view
    private CameraManager mCameraManager;

    private final CameraDevice.StateCallback mStateCallBack = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice camera) {
            mCameraOpenCloseLock.release();
            mCameraDevice = camera;
//            createCameraPreviewSession();
            takePreview();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {
            mCameraOpenCloseLock.release();
            camera.close();
            mCameraDevice = null;
        }

        @Override
        public void onError(@NonNull CameraDevice camera, int error) {
            mCameraOpenCloseLock.release();
            camera.close();
            mCameraDevice = null;
        }
    };

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String uri = "";
    private String mParam2;

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initSurfaceView2();
        btnCapture.setEnabled(true);
        btnCapture.setVisibility(View.VISIBLE);

        if(uri.isEmpty()){
            btnTest.setEnabled(false);
        }

        //getActivity().addContentView(customFrame, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//        initSurfaceView2();
    }

    @Override
    public void onResume() {
        super.onResume();

        // method 1
//        startBackgroundThread();
//
//        if(textureView.isAvailable()){
//            openCamera(textureView.getWidth(), textureView.getHeight());
//        }else{
//            textureView.setSurfaceTextureListener(mSurfaceTextureListener);
//        }
    }

    @Override
    public void onPause() {
        super.onPause();

        // method 1
//        closeCamera();
//        stopBackgroundThread();

    }

//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        closeCamera();
//        stopBackgroundThread();
//    }

    public void initSurfaceView(){
        transparentView.setZOrderOnTop(true);
        transparentViewHolder = transparentView.getHolder();
        transparentViewHolder.setFormat(PixelFormat.TRANSPARENT);

        transparentViewHolder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                Canvas canvas = holder.lockCanvas();

                int x0 = canvas.getWidth()/2;
                int y0 = canvas.getHeight()/2;
                int dx = canvas.getHeight()/3;
                int dy = canvas.getHeight()/3;

                if(canvas == null){
                    Toast.makeText(getContext(), "Canvas Null!", Toast.LENGTH_SHORT).show();
                }else{
                    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
                    paint.setColor(getResources().getColor(R.color.colorAccent));
                    paint.setStrokeWidth(4);
                    paint.setStyle(Paint.Style.STROKE);

                    canvas.drawRect(50,100,650,500, paint);
//                    canvas.drawRect(x0-dx, y0-dy, x0+dx, y0+dy, paint);
                    holder.unlockCanvasAndPost(canvas);
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {

            }
        });
    }

    public void initSurfaceView2(){
        cameraView.setKeepScreenOn(true);
        transparentView.setZOrderOnTop(true);

        cameraViewHolder = cameraView.getHolder();
        transparentViewHolder = transparentView.getHolder();

        transparentViewHolder.setFormat(PixelFormat.TRANSPARENT);

        cameraViewHolder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                initCamera2();
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                if(null != mCameraDevice){
                    mCameraDevice.close();
                    mCameraDevice = null;
                }

            }
        });

        transparentViewHolder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(@NonNull SurfaceHolder holder) {
                Canvas canvas = holder.lockCanvas();

                int x0 = canvas.getWidth()/2;
                int y0 = canvas.getHeight()/2;
                int dx = canvas.getHeight()/3;
                int dy = canvas.getHeight()/3;

                if(canvas == null){
                    Toast.makeText(getContext(), "Canvas Null!", Toast.LENGTH_SHORT).show();
                }else{
                    Paint paint = new Paint();
                    paint.setColor(getResources().getColor(R.color.colorAccent));
                    paint.setStrokeWidth(4);
                    paint.setStyle(Paint.Style.STROKE);

//                    rect = new Rect(50,100,650,500);
//                    rect = new Rect(50,300,650,800);

                    DisplayMetrics metrics = new DisplayMetrics();

                    getActivity().getDisplay().getRealMetrics(metrics);
                    int screenWidth = metrics.widthPixels;
                    int screenHeight = metrics.heightPixels;

                    int leftPercent = screenWidth * 80/100;
                    int rightPercent = screenWidth * 20/100 ;
                    int topPercent = screenHeight * 70/100;
                    int bottomPercent = screenHeight * 40/100;

                    int left = screenWidth - leftPercent;
                    int right = screenWidth - rightPercent;
                    int top = screenHeight - topPercent;
                    int bottom = screenHeight - bottomPercent;

                    Toast.makeText(getContext(), "Width = " + screenWidth + "\nHeight = " + screenHeight + "\nLeft = " + leftPercent + "\nRight = " + rightPercent,Toast.LENGTH_LONG).show();

                    rect = new Rect(left,top, right,bottom);

                    canvas.drawRect(rect, paint);
//                    canvas.drawRect(x0-dx, y0-dy, x0+dx, y0+dy, paint);
                    holder.unlockCanvasAndPost(canvas);
                }
            }

            @Override
            public void surfaceChanged(@NonNull SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(@NonNull SurfaceHolder holder) {

            }
        });


    }

    private void initCamera2() {
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());

        mainHandler = new Handler(Looper.getMainLooper());

        mCameraId = "" + CameraCharacteristics.LENS_FACING_FRONT;

        mImageReader = ImageReader.newInstance(1080,1920, ImageFormat.JPEG, 1);

        mImageReader.setOnImageAvailableListener(new ImageReader.OnImageAvailableListener() {
            @Override
            public void onImageAvailable(ImageReader reader) {

                mCameraDevice.close();
                transparentView.setVisibility(View.GONE);
                cameraView.setVisibility(View.GONE);
                imagePreview.setVisibility(View.VISIBLE);
//                cropImageView.setVisibility(View.VISIBLE);

                Image image = reader.acquireNextImage();

                ByteBuffer buffer = image.getPlanes()[0].getBuffer();

                byte[] bytes = new byte[buffer.remaining()];
                buffer.get(bytes);
                Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

                if(bmp != null){

                    Matrix matrix = new Matrix();

                    matrix.postRotate(90);
//                    matrix.postRotate(180);
//                    matrix.postRotate(270);

                    bmp = Bitmap.createBitmap(bmp, 0,0, bmp.getWidth(), bmp.getHeight(), matrix, true);

                    Bitmap newBmp = getCroppedBitmap(bmp);

//                    Toast.makeText(getContext(), "Width = " + newBmp.getWidth() + "\nHeight = " + newBmp.getHeight(), Toast.LENGTH_LONG).show();
//                    imagePreview.setImageBitmap(newBmp);

                    NavHostFragment navHostFragment = (NavHostFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment);
                    NavController navController = navHostFragment.getNavController();
                    NavDirections action = MainFragmentDirections.actionMainFragmentToResultFragment(uri).setImageUri(uri).setBitmap(newBmp);
                    navController.navigate(action);
                }


            }
        }, mainHandler);

        mCameraManager = (CameraManager) getActivity().getSystemService(Context.CAMERA_SERVICE);

        try {
            if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
                requestCameraPermission();
                return;
            }

            if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                requestWriteFileOnInternalStorage();
                return;
            }

            mCameraManager.openCamera(mCameraId, mStateCallBack, mainHandler);
        }catch(CameraAccessException e){
            e.printStackTrace();
        }

    }

    public Bitmap getCroppedBitmap(Bitmap bmp){
        Bitmap croppedBitmap = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(croppedBitmap);

        Paint paint = new Paint();
//        Rect rect = new Rect(0,0, bmp.getWidth(), bmp.getHeight());
        paint.setAntiAlias(true);
        paint.setColor(getResources().getColor(R.color.colorAccent));
        paint.setStrokeWidth(4);
        paint.setStyle(Paint.Style.STROKE);

        canvas.drawARGB(0,0,0,0);
//        canvas.drawRect(bmp.getWidth()/2, bmp.getHeight()/2, bmp.getWidth()/2, bmp.getHeight()/2 ,paint);

        canvas.drawRect(0, 0, bmp.getWidth(), bmp.getHeight(),paint);

//        Rect src = new Rect(rect.left - 50, rect.top - 50, rect.right, rect.bottom);
        Rect src = new Rect(rect.left + 80, rect.top - 60, rect.right + 240, rect.bottom - 140);

        Rect dst = new Rect(0, 0, bmp.getWidth(), bmp.getHeight());

        canvas.drawBitmap(bmp, src, dst, paint);

//        Toast.makeText(getContext(), String.valueOf(croppedBitmap.getWidth()), Toast.LENGTH_LONG).show();
        return croppedBitmap;
    }

    @OnClick(R.id.btnCapture)
    public void takePicture(){
        takePicture2();
    }

    public void takePicture2(){
        if(mCameraDevice == null){
            return;
        }

        try{
            mPreviewRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);

            mPreviewRequestBuilder.addTarget(mImageReader.getSurface());

            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
            //mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);

            int rotation = getActivity().getDisplay().getRotation();
            mPreviewRequestBuilder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get(rotation));
            CaptureRequest request = mPreviewRequestBuilder.build();

            mCaptureSession.capture(request, null, mBackgroundHandler);

        }catch(CameraAccessException e){
            e.printStackTrace();
        }



    }

    public void takePicture1(){
        if(null == mCameraDevice){
            Log.e("CAMERA 2", "cameraDevice is null");
            return;
        }
        CameraManager manager = (CameraManager) getActivity().getSystemService(Context.CAMERA_SERVICE);

        try {
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(mCameraDevice.getId());
            Size[] jpegSizes = null;
            if(characteristics != null){
                jpegSizes = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP).getOutputSizes(ImageFormat.JPEG);
            }

            int width = 640;
            int height = 480;
            if(jpegSizes != null && 0 < jpegSizes.length){
                width = jpegSizes[0].getWidth();
                height = jpegSizes[0].getHeight();
            }

            ImageReader reader = ImageReader.newInstance(width, height, ImageFormat.JPEG, 1);

            List<Surface> outputSurfaces = new ArrayList<>(2);
            outputSurfaces.add(reader.getSurface());
            outputSurfaces.add(new Surface(textureView.getSurfaceTexture()));

            final CaptureRequest.Builder captureBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureBuilder.addTarget(reader.getSurface());
            captureBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);

            // orientation
            //int rotation = getActivity().getWindowManager().getDefaultDisplay().getRotation();

            int rotation = getContext().getDisplay().getRotation();
            captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get(rotation));
//            captureBuilder.set(CaptureRequest.JPEG_ORIENTATION,-90);

            // nama file dengan tanggal
            Date captureDate = new Date();
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            String captureDateFormatted = format.format(captureDate);

            //final File file = new File(Environment.getExternalStorageDirectory() + "/TessOcrAdit_" + captureDateFormatted + ".jpg");

            String filePathEnvironment = Environment.getExternalStorageDirectory() + "";
            String filePathGetFilesDir = getContext().getExternalFilesDir(null) + "";

            File file = new File(filePathGetFilesDir + File.separator + "TesseractAdit_" + captureDateFormatted + ".jpg");

            ImageReader.OnImageAvailableListener readerListener = new ImageReader.OnImageAvailableListener(){

                @Override
                public void onImageAvailable(ImageReader reader) {
                    Image image = null;

                    try {
                        image = reader.acquireLatestImage();
                        ByteBuffer buffer = image.getPlanes()[0].getBuffer();
                        byte[] bytes = new byte[buffer.capacity()];
                        buffer.get(bytes);
                        save(bytes);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        if(image != null){
                            image.close();
                        }
                    }
                }

                private void save(byte[] bytes) throws IOException {
                    OutputStream output = null;
                    try {
                        output = new FileOutputStream(file);
                        output.write(bytes);
                    } finally {
                        if (null != output) {
                            output.close();
                        }
                    }
                }
            };

            reader.setOnImageAvailableListener(readerListener, mBackgroundHandler);

            final CameraCaptureSession.CaptureCallback captureListener = new CameraCaptureSession.CaptureCallback() {
                @Override
                public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
                    super.onCaptureCompleted(session, request, result);
                    //Toast.makeText(getActivity(), "Foto disimpan :" + file, Toast.LENGTH_LONG).show();
                    // pindah ke fragment result
                    //String uri = Environment.getExternalStorageDirectory() + "/TessOcrAdit_" + captureDateFormatted + ".jpg";

                    uri = String.valueOf(file);
                    //goToResult(uri);
                    Toast.makeText(getActivity(), "URI :" + uri, Toast.LENGTH_LONG).show();

//                    createCameraPreviewSession();
                }
            };

            mCameraDevice.createCaptureSession(outputSurfaces, new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession session) {
                    try {
                        session.capture(captureBuilder.build(), captureListener, mBackgroundHandler);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession session) {

                }
            }, mBackgroundHandler);


        } catch (CameraAccessException e) {
            e.printStackTrace();
        } finally {
            btnTest.setEnabled(true);
            btnTest.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);

            handler = new Handler();

            runnable = new Runnable() {

                @Override
                public void run() {

                    goToResult();
                }
            };

            handler.postDelayed(runnable, 3000);
        }
    }

    private void closeCamera() {
        try {
            mCameraOpenCloseLock.acquire();
            if(null != mCaptureSession){
                mCaptureSession.close();
                mCaptureSession = null;
            }
            if(null != mCameraDevice){
                mCameraDevice.close();
                mCameraDevice = null;
            }
            if(null != mImageReader){
                mImageReader.close();
                mImageReader = null;
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Timeout waiting to lock camera opening" ,e);
        } finally {
            mCameraOpenCloseLock.release();
        }
    }

//    @OnClick(R.id.btnTest)
//    public void goToResult(){
//        //NavDirections action = TessFragment2Directions.tessFragment2toResultFragment().setFileUri(uri);
//        //NavHostFragment.findNavController(this).navigate(action);
//
//        NavHostFragment navHostFragment = (NavHostFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment);
//        NavController navController = navHostFragment.getNavController();
//        NavDirections action = MainFragmentDirections.actionMainFragmentToResultFragment(uri);
//        navController.navigate(action);
//        //NavHostFragment.findNavController(this).navigate(action);
//    }

    public void goToResult(){
        //NavDirections action = TessFragment2Directions.tessFragment2toResultFragment().setFileUri(uri);
        //NavHostFragment.findNavController(this).navigate(action);
        progressBar.setVisibility(View.INVISIBLE);

        NavHostFragment navHostFragment = (NavHostFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment);
        NavController navController = navHostFragment.getNavController();
        NavDirections action = MainFragmentDirections.actionMainFragmentToResultFragment(uri).setImageUri(uri);
        navController.navigate(action);

        //Toast.makeText(getActivity(), "URI :" + uriString, Toast.LENGTH_LONG).show();
    }

    private void openCamera(int width, int height){
        if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            requestCameraPermission();
            return;
        }

        if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            requestWriteFileOnInternalStorage();
            return;
        }

        setUpCameraOutputs(width, height);
        configureTransform(width, height);
        CameraManager manager = (CameraManager) getActivity().getSystemService(Context.CAMERA_SERVICE);

        try {
            if(!mCameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)){
                throw new RuntimeException("Timeout waiting to lock camera opening");
            }
            manager.openCamera(mCameraId, mStateCallBack, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }catch (InterruptedException e) {
            throw new RuntimeException("Timeout waiting to lock camera opening" ,e);
        }
    }

    private void setUpCameraOutputs(int width, int height) {
        CameraManager manager = (CameraManager) getActivity().getSystemService(Context.CAMERA_SERVICE);

        try {
            for(String cameraId : manager.getCameraIdList()){
                CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);

                Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);
                if(facing != null && facing == CameraCharacteristics.LENS_FACING_FRONT){
                    continue;
                }

                StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                if(map == null){
                    continue;
                }

//                Size largest = Collections.max(Arrays.asList(map.getOutputSizes(ImageFormat.JPEG)), new CompareSizesByArea());
//                mImageReader = ImageReader.newInstance(largest.getWidth(), largest.getHeight(), ImageFormat.JPEG, 1);

                Size largest = Collections.max(Arrays.asList(map.getOutputSizes(ImageFormat.JPEG)), new CompareSizesByArea());
                mImageReader = ImageReader.newInstance(largest.getWidth(), largest.getHeight(), ImageFormat.JPEG, 1);

//                Size largest = Collections.max(Arrays.asList(map.getOutputSizes(ImageFormat.YUV_422_888)), new CompareSizesByArea());
//                mImageReader = ImageReader.newInstance(largest.getWidth(), largest.getHeight(), ImageFormat.YUV_422_888, 2);

                mImageReader.setOnImageAvailableListener(null, mBackgroundHandler);

                Point displaySize = new Point();
//                getActivity().getWindowManager().getDefaultDisplay().getSize(displaySize);
                getActivity().getDisplay().getRealSize(displaySize);

                int rotatedPreviewWidth = width;
                int rotatedPreviewHeight = height;
                int maxPreviewWidth = displaySize.x;
                int maxPreviewHeight = displaySize.y;

                if(maxPreviewWidth > MAX_PREVIEW_WIDTH){
                    maxPreviewWidth = MAX_PREVIEW_WIDTH;
                }
                if(maxPreviewHeight > MAX_PREVIEW_HEIGHT){
                    maxPreviewHeight = MAX_PREVIEW_HEIGHT;
                }

                mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class),
                        rotatedPreviewWidth, rotatedPreviewHeight, maxPreviewWidth, maxPreviewHeight, largest);
                mCameraId = cameraId;
                return;
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (NullPointerException e){
            Toast.makeText(getActivity(), "CAMERA 2 API IS NOT SUPPORTED on THIS DEVICE", Toast.LENGTH_LONG).show();
        }
    }

    private void createCameraPreviewSession() {
        try {
            SurfaceTexture texture = textureView.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
            Surface surface = new Surface(texture);

            mPreviewRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            mPreviewRequestBuilder.addTarget(surface);

            mCameraDevice.createCaptureSession(Arrays.asList(surface, mImageReader.getSurface()), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession session) {
                    if(null == mCameraDevice){
                        return;
                    }

                    mCaptureSession = session;

                    try {
                        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);

                        mPreviewRequest = mPreviewRequestBuilder.build();

                        mCaptureSession.setRepeatingRequest(mPreviewRequest, null, mBackgroundHandler);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                    Toast.makeText(getActivity(), "Failed", Toast.LENGTH_LONG);
                }
            }, null);

        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void takePreview(){
        try {

            mPreviewRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            mPreviewRequestBuilder.addTarget(cameraViewHolder.getSurface());

            mCameraDevice.createCaptureSession(Arrays.asList(cameraViewHolder.getSurface(), mImageReader.getSurface()), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession session) {
                    if(null == mCameraDevice){
                        return;
                    }

                    mCaptureSession = session;

                    try {
                        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);

                        mPreviewRequest = mPreviewRequestBuilder.build();

                        mCaptureSession.setRepeatingRequest(mPreviewRequest, null, mBackgroundHandler);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                    Toast.makeText(getActivity(), "Failed", Toast.LENGTH_LONG);
                }
            }, null);

        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void requestCameraPermission(){
        if(ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA)){
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
        }else{
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
        }
    }

    private void requestWriteFileOnInternalStorage(){
        if(ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)){
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_EXTERNAL_STORAGE);
        }else{
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_EXTERNAL_STORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == REQUEST_CAMERA_PERMISSION){
            if(grantResults.length != 1 || grantResults[0] != PackageManager.PERMISSION_GRANTED){
                Toast.makeText(getActivity(), "Camera Permission not Granted!", Toast.LENGTH_LONG).show();
            }
        }else{
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void startBackgroundThread(){
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    private void stopBackgroundThread(){
        mBackgroundThread.quitSafely();

        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static Size chooseOptimalSize(Size[] choices, int textureViewWidth, int textureViewHeight,
                                          int maxWidth, int maxHeight, Size aspectRatio){
        List<Size> bigEnough = new ArrayList<>();
        List<Size> notBigEnough = new ArrayList<>();
        int w = aspectRatio.getWidth();
        int h = aspectRatio.getHeight();
        for(Size option : choices){
            if(option.getWidth() <= maxWidth && option.getHeight() <= maxHeight &&
                    option.getHeight() == option.getWidth() * h / w){
                if(option.getWidth() >= textureViewWidth && option.getHeight() >= textureViewHeight){
                    bigEnough.add(option);
                }else{
                    notBigEnough.add(option);
                }
            }
        }

        if(bigEnough.size() > 0){
            return Collections.min(bigEnough, new CompareSizesByArea());
        }else if(notBigEnough.size() > 0){
            return Collections.max(notBigEnough, new CompareSizesByArea());
        }else{
            Log.e("CAMERA 2", " Couldnt find any suitable preview size!");
            return choices[0];
        }
    }

    private void configureTransform(int viewWidth, int viewHeight){
        if(null == mTextureView || null == mPreviewSize){
            return;
        }

//        int rotation = getActivity().getWindowManager().getDefaultDisplay().getRotation();

        int rotation = getContext().getDisplay().getRotation();

        Matrix matrix = new Matrix();
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        RectF bufferRect = new RectF(0, 0, mPreviewSize.getHeight(), mPreviewSize.getWidth());
        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();

        if(Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation){
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
            float scale = Math.max((float) viewHeight / mPreviewSize.getHeight(), (float) viewWidth / mPreviewSize.getWidth());
            matrix.postScale(scale, scale, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
        }else if(Surface.ROTATION_180 == rotation){
            matrix.postRotate(180, centerX, centerY);
        }
        textureView.setTransform(matrix);
    }

    static class CompareSizesByArea implements Comparator<Size> {

        @Override
        public int compare(Size o1, Size o2) {
            return Long.signum((long) o1.getWidth() * o1.getHeight() - (long) o2.getWidth() * o2.getHeight());
        }
    }

}